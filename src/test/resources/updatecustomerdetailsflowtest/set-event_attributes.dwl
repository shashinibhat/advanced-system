{
  "headers": {
    "content-type": "application/json",
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "postman-token": "0b42a8b5-accf-4041-b7d3-d3f4624d42d1",
    "host": "localhost:8086",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "content-length": "192"
  },
  "clientCertificate": null,
  "method": "PUT",
  "scheme": "http",
  "queryParams": {},
  "requestUri": "/api/customers",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/customers",
  "listenerPath": "/api/*",
  "relativePath": "/api/customers",
  "localAddress": "/127.0.0.1:8086",
  "uriParams": {},
  "rawRequestUri": "/api/customers",
  "rawRequestPath": "/api/customers",
  "remoteAddress": "/127.0.0.1:53637",
  "requestPath": "/api/customers"
}