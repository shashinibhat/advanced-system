{
  "headers": {
    "content-type": "application/json",
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "postman-token": "8b4562c4-85c4-424d-a670-143948b33f46",
    "host": "localhost:8086",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "content-length": "192"
  },
  "clientCertificate": null,
  "method": "POST",
  "scheme": "http",
  "queryParams": {},
  "requestUri": "/api/customers",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/customers",
  "listenerPath": "/api/*",
  "relativePath": "/api/customers",
  "localAddress": "/127.0.0.1:8086",
  "uriParams": {},
  "rawRequestUri": "/api/customers",
  "rawRequestPath": "/api/customers",
  "remoteAddress": "/127.0.0.1:53563",
  "requestPath": "/api/customers"
}