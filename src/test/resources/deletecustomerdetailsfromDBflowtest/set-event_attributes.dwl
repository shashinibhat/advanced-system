{
  "headers": {
    "content-type": "application/json",
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "postman-token": "270f666d-afe1-45f4-a828-4dc0dd4dff98",
    "host": "localhost:8086",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "content-length": "192"
  },
  "clientCertificate": null,
  "method": "DELETE",
  "scheme": "http",
  "queryParams": {},
  "requestUri": "/api/customers/2",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/customers/2",
  "listenerPath": "/api/*",
  "relativePath": "/api/customers/2",
  "localAddress": "/127.0.0.1:8086",
  "uriParams": {
    "ID": "2"
  },
  "rawRequestUri": "/api/customers/2",
  "rawRequestPath": "/api/customers/2",
  "remoteAddress": "/127.0.0.1:53709",
  "requestPath": "/api/customers/2"
}