{
  "headers": {
    "content-type": "application/json",
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "postman-token": "c433cfcd-a7bb-4dc5-b819-aa568d1a016e",
    "host": "localhost:8086",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "content-length": "192"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "http",
  "queryParams": {},
  "requestUri": "/api/customers/1",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/customers/1",
  "listenerPath": "/api/*",
  "relativePath": "/api/customers/1",
  "localAddress": "/127.0.0.1:8086",
  "uriParams": {
    "ID": "1"
  },
  "rawRequestUri": "/api/customers/1",
  "rawRequestPath": "/api/customers/1",
  "remoteAddress": "/127.0.0.1:53867",
  "requestPath": "/api/customers/1"
}