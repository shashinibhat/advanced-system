%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "firstName": "Sayeed",
    "address": "Bangalore",
    "phoneNumber": 85786543,
    "customer_id": 1,
    "age": 55,
    "email": "sayeed.p.chand@apisero.com",
    "lastName": "P"
  }
])