{
  "headers": {
    "content-type": "application/json",
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "postman-token": "1cb2a7ff-6593-43dd-b868-f2863bc4a072",
    "host": "localhost:8086",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "content-length": "192"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "http",
  "queryParams": {},
  "requestUri": "/api/customers",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/customers",
  "listenerPath": "/api/*",
  "relativePath": "/api/customers",
  "localAddress": "/127.0.0.1:8086",
  "uriParams": {},
  "rawRequestUri": "/api/customers",
  "rawRequestPath": "/api/customers",
  "remoteAddress": "/127.0.0.1:53774",
  "requestPath": "/api/customers"
}